package com.example.sparrow;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class GameEngine extends SurfaceView implements Runnable {
    private final String TAG = "SPARROW";

    // game thread variables
    private Thread gameThread = null;
    private volatile boolean gameIsRunning;

    // drawing variables
    private Canvas canvas;
    private Paint paintbrush;
    private SurfaceHolder holder;

    // Screen resolution varaibles
    private int screenWidth;
    private int screenHeight;

    // VISIBLE GAME PLAY AREA

    int VISIBLE_LEFT;
    int VISIBLE_TOP;
    int VISIBLE_RIGHT;
    int VISIBLE_BOTTOM;

    // SPRITES
    Square bullet;
    int SQUARE_WIDTH = 100;

    Square enemy;

    Sprite player;
    Sprite sparrow;
    int sparrowX;
    int sparrowY;

    Sprite cat;



    ArrayList<Square> bullets = new ArrayList<Square>();
    //cat variables
    Bitmap catImage;
    Rect catHitbox;
    Point cagePosition;
    Point catPosition;
    final int DISTANCE_FROM_BOTTOM = 350;
    final int CAGE_WIDTH = 300;
    final int CAGE_HEIGHT = 200;

    // GAME STATS
    int score = 0;

    public GameEngine(Context context, int screenW, int screenH) {
        super(context);

        // intialize the drawing variables
        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.bullet = new Square(context,100,600, SQUARE_WIDTH);

        // set screen height and width
        this.screenWidth = screenW;
        this.screenHeight = screenH;

        // setup visible game play area variables
        this.VISIBLE_LEFT = 20;
        this.VISIBLE_TOP = 10;
        this.VISIBLE_RIGHT = this.screenWidth - 20;
        this.VISIBLE_BOTTOM = (int) (this.screenHeight * 0.8);


        // initalize sprites

        this.catPosition = new Point();
        this.catPosition.x = VISIBLE_RIGHT-400 ;
        this.catPosition.y = VISIBLE_BOTTOM -300;

        this.player = new Sprite(this.getContext(), 100, 700, R.drawable.player64);
        this.sparrow = new Sprite(this.getContext(), 500, 200, R.drawable.bird64);
        this.cat = new Sprite(this.getContext(), catPosition.x, catPosition.y, R.drawable.cat64);

        //setup cat hitbox
        //this.catImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.cat64);
        //this.catHitbox = new Rect(
                //this.catPosition.x,
               // this.catPosition.y,
                //this.catPosition.x+this.catPosition.getwidth();
                //this.catPosition.y + catPosition.getHeight();
        // racket
        cagePosition = new Point();
        cagePosition.x = VISIBLE_RIGHT - CAGE_WIDTH;   // left
        cagePosition.y =  VISIBLE_TOP + CAGE_HEIGHT;

    }

    @Override
    public void run() {
        while (gameIsRunning == true) {
            updateGame();    // updating positions of stuff

            redrawSprites(); // drawing the stuff
            updatePositions();
            controlFPS();
        }
    }


    boolean movingRight = true;
    final int CAGE_SPEED = 50;
    final int CAT_SPEED = 20;

    // Game Loop methods
    public void updateGame() {
        Random r = new Random();
        int randomX = r.nextInt(this.screenWidth) + 1;
        int randomY = r.nextInt(this.screenHeight) + 1;


        // @TODO: Update the position of the sprites


        //move cage
        if (movingRight == true) {
            cagePosition.x = cagePosition.x + CAGE_SPEED;

        }
        else {
            cagePosition.x = cagePosition.x - CAGE_SPEED;
        }

        // @TODO: Collision detection code
        if (cagePosition.x > VISIBLE_RIGHT) {
            Log.d(TAG, "CAGE reached right of screen. Changing direction!");
            movingRight = false;
        }

        if (cagePosition.x < 0) {
            Log.d(TAG, "CAGE reached left of screen. Changing direction!");
            movingRight = true;
        }

        Log.d(TAG, "CAGE x-position: " + cagePosition.x);



     //move cat
        if (movingRight == true) {
            this.catPosition.x = this.catPosition.x + CAT_SPEED;

        }
        else {
            this.catPosition.x = this.catPosition.x - CAT_SPEED;
        }

        // @TODO: Collision detection code
        if (this.catPosition.x > VISIBLE_RIGHT) {
            Log.d(TAG, "cat reached right of screen. Changing direction!");
            movingRight = false;
        }

        if (this.catPosition.x < VISIBLE_LEFT) {
            Log.d(TAG, "cat reached left of screen. Changing direction!");
            movingRight = true;
        }

        Log.d(TAG, "cat x-position: " + catPosition.x);

        //draw bullets

        Log.d(TAG,"Bullet position: " + this.bullet.getxPosition() + ", " + this.bullet.getyPosition());
        Log.d(TAG,"Enemy position: " + this.cagePosition.x + ", " + this.cagePosition.y);



        // 1. calculate distance between bullet and enemy
        double a = this.cagePosition.x - this.bullet.getxPosition();
        double b = this.cagePosition.y - this.bullet.getyPosition();


        double d = Math.sqrt((a * a) + (b * b));

        Log.d(TAG, "Distance to enemy: " + d);

        // 2. calculate xn and yn constants
        // (amount of x to move, amount of y to move)
        double xn = (a / d);
        double yn = (b / d);

        // 3. calculate new (x,y) coordinates
        int newX = this.bullet.getxPosition() + (int) (xn * 15);
        int newY = this.bullet.getyPosition() + (int) (yn * 15);
        this.bullet.setxPosition(newX);
        this.bullet.setyPosition(newY);

    }


    public void outputVisibleArea() {
        Log.d(TAG, "DEBUG: The visible area of the screen is:");
        Log.d(TAG, "DEBUG: Maximum w,h = " + this.screenWidth +  "," + this.screenHeight);
        Log.d(TAG, "DEBUG: Visible w,h =" + VISIBLE_RIGHT + "," + VISIBLE_BOTTOM);
        Log.d(TAG, "-------------------------------------");
    }



    public void updatePositions() {

        Random bullet = new Random();


        this.sparrowX = bullet.nextInt(this.screenWidth-100);
        this.sparrowY = bullet.nextInt(this.screenHeight-100);
        Log.d(TAG, "SPARROW: x=" + this.sparrowX + "Y:" + this.sparrowY);



    }



    public void redrawSprites() {
        if (holder.getSurface().isValid()) {

            // initialize the canvas
            canvas = holder.lockCanvas();
            // --------------------------------

            // set the game's background color
            canvas.drawColor(Color.argb(255,255,255,255));

            // setup stroke style and width
            paintbrush.setStyle(Paint.Style.FILL);
            paintbrush.setStrokeWidth(8);

            // --------------------------------------------------------
            // draw boundaries of the visible space of app
            // --------------------------------------------------------
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setColor(Color.argb(255, 0, 128, 0));

            canvas.drawRect(VISIBLE_LEFT, VISIBLE_TOP, VISIBLE_RIGHT, VISIBLE_BOTTOM, paintbrush);
            this.outputVisibleArea();

            // --------------------------------------------------------
            // draw player and sparrow
            // --------------------------------------------------------

            // 1. player
            canvas.drawBitmap(this.player.getImage(), this.player.getxPosition(), this.player.getyPosition(), paintbrush);

            // 2. sparrow
            canvas.drawBitmap(this.sparrow.getImage(), this.sparrow.getxPosition(), this.sparrow.getyPosition(), paintbrush);

            // 3. cat



            canvas.drawBitmap(this.cat.getImage(), this.catPosition.x, this.catPosition.y, paintbrush);

            //4. racket

            // cage diagram
            int paddleLeft = cagePosition.x;
            int paddleTop = cagePosition.y;
            int paddleRight = cagePosition.x + 2*CAGE_WIDTH;
            int paddleBottom = cagePosition.y + CAGE_HEIGHT;
            canvas.drawRect(paddleLeft, paddleTop, paddleRight, paddleBottom, paintbrush);


            // --------------------------------------------------------
            // draw hitbox on cat
            // --------------------------------------------------------
            paintbrush.setColor(Color.BLUE);
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setStrokeWidth(5);


            // --------------------------------------------------------
            // draw hitbox on player
            // --------------------------------------------------------
            paintbrush.setTextSize(60);
            paintbrush.setStrokeWidth(5);
            String screenInfo = "Screen size: (" + this.screenWidth + "," + this.screenHeight + ")";
            canvas.drawText(screenInfo, 10, 100, paintbrush);



            // draw bullet
            paintbrush.setColor(Color.RED);
            canvas.drawRect(
                    this.bullet.getxPosition(),
                    this.bullet.getyPosition(),
                    this.bullet.getxPosition() + this.bullet.getWidth(),
                    this.bullet.getyPosition() + this.bullet.getWidth(),
                    paintbrush
            );

            // --------------------------------
            holder.unlockCanvasAndPost(canvas);
        }

    }

    public void controlFPS() {
        try {
            gameThread.sleep(17);
        }
        catch (InterruptedException e) {

        }
    }


    // Deal with user input
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_UP:
                break;
            case MotionEvent.ACTION_DOWN:
                break;
        }
        return true;
    }

    // Game status - pause & resume
    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        }
        catch (InterruptedException e) {

        }
    }
    public void  resumeGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

}
